package com.bakaeva.tm;

import com.bakaeva.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** Welcome to task manager ***");
        parseArgs(args);
    }
    
    private static void parseArgs(final String[] args){
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
    }

    private static void showHelp(){
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - show developer info.");
        System.out.println(TerminalConst.VERSION + " - show version info.");
        System.out.println(TerminalConst.HELP + " - show terminal commands.");
    }

    private static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("Name: Alina Bakaeva");
        System.out.println("E-mail: Bak_Al@bk.ru");
    }

    private static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
